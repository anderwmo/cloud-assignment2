
## Scenario

Hammerfest hospital is a small hospital consisting of two buildings situated at Kvaløya in the northern part of norway. They have recieved funding to implement IoT devices installed in every room used for patients. The sensors installed are temperature,- humidity,- and pulse-sensors as well as an alarm which can be used to call for immedaite help. These sensors are monitored by the maintenance crew, who is in charge of monitoring both temperature and humidity, and the nurses who monitors the patients in both building A and B.


## Architecture 

#### Achitecture of the scenario:

![Alt text](scenario.png?raw=true "Scenario")

## Requirements
The health personnel need to recieve constant updated on the pulse of the patients in order to monitor thir status to enable them to react fast if something special happens. This data does not need to be stored in the database as it is more to be used in the moment. They also need to recieve updates from the alarm in order to react to requests from patients. This should be stored in the database in order to keep track of number of incidents and to keep a record. 

The maintenance crew needs to keep records of the climate inside the hospital. this data is sent to the crew for constant monitoring as well as stored in the database to keep track of logs. 

#### System architecture:

![Alt text](system.png?raw=true "Scenario")


## Topics
The topics is divided into the categories health and maintenance and further specified into building and room.

### Publishers
To simulate this environment we generate sensor data for only two rooms in building A and B. The system works when simulating all 20 rooms, but this will generate so much data it makes it hhard to see what is going on.

The first publisher simulates all the temperature and humidity sensors placed inn all the rooms. This node sends data in XML format to the broker with topic `maintenance/building[A:B]/room[1-10]/temperature`. Data is sent every 10 seconds with information as of right now and for 5 seconds ago.

The second publisher simulates pulse sensors which is equipped by each patient in every room. This data is converted to EXI and sent to the broker with topic `health/building[A:B]/room[1-10]/pulse`. Data is sent every 10 seconds with information about the patients hearthbeat as of right now.

The third publisher simulates an alarm that reports every 15 seconds if it is pressed or not. Data is sent in JSON format to the broker with topic `health/building[A:B]/room[1-10]/alarm`


### Subsribers

The maintenance crew is subscribed to `maintenance/#`  
Nurse team A is subscribed to `health/buildingA/#`  
Nurse team B is subscribed to `health/buildingB/#`  



## Deployment guide

- Requirements:
	- Docker
	- Docker-compose

#### How to install docker for ubuntu
https://docs.docker.com/engine/install/ubuntu/

TLDR script;
```
#!/bin/bash
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y apt-transport-https ca-certificates curl gnupg lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y docker-ce docker-ce-cli containerd.io
```
#### How to install docker-compose
https://docs.docker.com/compose/install/

TLDR commands;
```
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

### Services that will be created
This creates the following services:

+ docker_subscriber_building_a_1          
+ docker_publisher_pulse_1                
+ docker_publisher_temperature_humidity_1 
+ docker_subscriber_building_b_1          
+ docker_publisher_alarm_1                        
+ docker_subscriber_maintenance_1        
+ docker_broker_1                     


### Start containers
Method 1: Start containers and view all logs simultaneously
```
git clone https://bitbucket.org/anderwmo/cloud-assignment2.git
cd assignment2/docker
sudo docker-compose up --build 
```
Method 2: Start containers and view no logs
```
git clone https://bitbucket.org/anderwmo/cloud-assignment2.git
cd assignment2/docker
sudo docker-compose up --build -d 
```

### Stop containers
Stop containers using method 1 for starting:  
ctrl+c

Stop containers using method 2 for starting:  
```
sudo docker-compose down
```

### Inspecting container set up using method 2
sudo docker logs -t -f <name of service>
