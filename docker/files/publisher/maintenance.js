const mqtt = require("mqtt");

const client = mqtt.connect("mqtt://broker:1883");


var convert = require('xml-js');


function getNumber(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


/*
Example senml data represented in JSON format

[{"bn":"buildingA_room2","bt":1.320067464e+09,
      "bu":"Cel","v":20},
     {"u":"%RH","v":60},
     {"t":50,"v":21},
     {"u":"%RH","v":59,"t":50}]
*/

client.on("connect", ack => {
	console.log("connected!");

var intervalId = setInterval(function(){
for (j = 0; j <= 1; j++) { //For both building A and B

	var aorb = (j == 1) ? "A" : "B";
for (i = 1; i <= 2; i++) { //For each room

	//Constructing the message
var message = '[{"bn":"' + 'building' + aorb + '_room' + i + '","bt":' + Date.now() + ',"bu":"Cel","v":' + getNumber(18,25) + '},{"u":"%RH","v":' + getNumber(30,50) + '},{"t":5,"v":' + getNumber(18,25) + '},{"u":"%RH","v":' + getNumber(30,50) + ',"t":50}]';
console.log(message);
var options = {compact: true};
var res = convert.json2xml(message, options);
//Hack since our library uses numbers as tags which is not supported, we also need to add some tags to obtain a valid xml format
//res = res.replace(/[<]\d[/][>]/g, '</element>')
res = res.replace(/[<]\d[>]/g, '<element>')
res = res.replace('</0>', '</element>')
res = res.replace('</1>', '</element>')
res = res.replace('</2>', '</element>')
res = res.replace('</3>', '</element>')
var open = '<?xml version="1.0" encoding="UTF-8"?><root>';
var close = '</root>';
var res = open.concat(res, close)
/////// Hack ended

client.publish('maintenance/building'+aorb+'/room'+i+'/temperature', res);
} }
}, 10000);
});
