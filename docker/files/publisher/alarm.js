const mqtt = require("mqtt");

const client = mqtt.connect("mqtt://broker:1883");


function probability(n) {
	return Math.random() < n;
}

//Example senML
//{"n":"rom1","vb":0,"t":1.320067464e+09}]
//{"n":"rom1","vb":1,"t":1.320067464e+09}]
client.on("connect", ack => {
	console.log("connected!");

var intervalId = setInterval(function(){
for (j = 0; j <= 1; j++) { //For both building A and B
	var aorb = (j == 1) ? "A" : "B";
for (i = 1; i <= 2; i++) { //For each room
	//Constructing the message
var alarm = (probability(0.2)) ? 1 : 0; //20% chance alarm is signaled
var message = '[{"n":"' + 'building' + aorb + '_room' + i + '","vb":'+ alarm + '","t":' + Date.now() + '}]'
client.publish('health/building'+aorb+'/room'+i+'/alarm', message); 
} }
}, 15000);
});
