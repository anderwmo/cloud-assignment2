const mqtt = require("mqtt");

const client = mqtt.connect("mqtt://broker:1883");

exi = require('exificient.js');

function getNumber(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/*
Example message:

[{"n":"rom1","u":"beat/min","v":62,"t":1.320067464e+09}]
*/




client.on("connect", ack => {
	console.log("connected!");

var intervalId = setInterval(function(){
for (j = 0; j <= 1; j++) { //For both building A and B

	var aorb = (j == 1) ? "A" : "B";
for (i = 1; i <= 2; i++) { //For each room
	//Constructing the message
name = ("building" + aorb + "_room" + i)

var message = [{"n": name,"u":"beat/min","v": getNumber(40,80),"t": Date.now()}]


var encoded = exi.exify(message);
console.log(encoded)

client.publish('health/building'+aorb+'/room'+i+'/pulse', encoded); 
} }
}, 10000);
});
