
//This subscibes to everything related to health in building A
//  
//

const mqtt = require("mqtt");

const client = mqtt.connect("mqtt://broker:1883");

var exi = require('exificient.js');


var topic = "health/buildingA/#";

// ON connect
client.on("connect", ack => {
  console.log("connected!");
  // console.log(ack);
  client.subscribe(topic, err => {
    console.log(err);
  });

  client.on("message", (topic, message) => {
    messagestring = message.toString()
  
// if data recieved is exi
    if (!messagestring.includes("building")) {  // EXI data does not contain the string building
      var decoded = exi.parse(message);
      var jsonstring = JSON.stringify(decoded)
      console.log(jsonstring);
      //
    } else {
	    console.log(messagestring);
    }
 });
});
