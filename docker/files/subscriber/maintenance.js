const mqtt = require("mqtt");

const client = mqtt.connect("mqtt://broker:1883");

const xml2js = require('xml2js');

var topic = "maintenance/#";

client.on("connect", ack => {
  console.log("connected!");
  // console.log(ack);
  client.subscribe(topic, err => {
    console.log(err);
  });
 
// Should recieve data as XML 
client.on("message", (topic, message) => {
  const xml = message.toString()
  xml2js.parseString(xml, (err, result) => {
    if(err) {
      throw err;
    } // result should be a JS object. Convert it to  JSON string
    const json = JSON.stringify(result.root.element, null, 0); 
    // string magic to make outlike like input
    var out  = json.replace(/[\[\]']+/g,'')
    var fin = '['
    fin = fin.concat(out, ']')
    console.log(fin);
});     
});});
